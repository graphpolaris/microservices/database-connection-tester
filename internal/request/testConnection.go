package request

// TestConnection describes the body of a POST request to the connectionTestHandler
type TestConnection struct {
	DatabaseType         string `json:"databaseType" validate:"required"`         // The type of database (arangodb, neo4j)
	Hostname             string `json:"hostname" validate:"required,url"`         // Address of the database
	Port                 int    `json:"port" validate:"required,gte=0,lte=65535"` // Port of the database
	Username             string `json:"username" validate:"required"`
	Password             string `json:"password" validate:"required"`
	InternalDatabaseName string `json:"internalDatabaseName" validate:"required"` // Internal database we should have READ access to
}
