package connectiontester

import "errors"

var (
	// ErrUnknown is thrown if the issue preventing connection is not known
	ErrUnknown = errors.New("unknown error preventing connection")

	// ErrUnauthorized is thrown if we are not authorized
	ErrUnauthorized = errors.New("invalid credentials, or we don't have access to the internal database")

	// ErrBadAddress is thrown if there is a connection timeout
	ErrBadAddress = errors.New("incorrect database address or port")

	// ErrInvalidProtocol is thrown if the address contains an invalid protocol
	ErrInvalidProtocol = errors.New("invalid protocol for this database type")
)
