package connectiontester

import (
	"context"
	"database-connection-tester/internal/request"
	"errors"
)

// ConnectionTester describes the methods a connection tester should implement
type ConnectionTester interface {
	TestConnection(ctx context.Context, req request.TestConnection) (err error)
}

// New creates a new ConnectionTester for the given databaseType
func New(databaseType string) (ct ConnectionTester, err error) {
	switch databaseType {
	case "arangodb":
		return &arangoConnectionTester{}, nil
	case "neo4j":
		return &neo4jConnectionTester{}, nil
	default:
		return nil, errors.New("invalid database type")
	}
}
