package connectiontester

import (
	"context"
	"database-connection-tester/internal/request"
	"fmt"
	"strings"

	"github.com/neo4j/neo4j-go-driver/v4/neo4j"
)

type neo4jConnectionTester struct {
}

func (ct *neo4jConnectionTester) TestConnection(ctx context.Context, req request.TestConnection) (err error) {
	driver, err := neo4j.NewDriver(fmt.Sprintf("%s:%d", req.Hostname, req.Port), neo4j.BasicAuth(req.Username, req.Password, ""))
	if err != nil {
		return
	}
	defer driver.Close()

	err = verifyConnectivity(driver, req.InternalDatabaseName)
	if neo4j.IsConnectivityError(err) {
		// Bad address
		if strings.Contains(err.Error(), "too many colons in address") {
			return ErrInvalidProtocol
		}
		// Bad address
		if strings.Contains(err.Error(), "Unable to retrieve routing table") {
			return ErrBadAddress
		}
		// Bad credentials
		if strings.Contains(err.Error(), "Neo.ClientError.Security.Unauthorized") {
			return ErrUnauthorized
		}
		return ErrBadAddress
	}

	// Create session
	session := driver.NewSession(neo4j.SessionConfig{
		DatabaseName: req.InternalDatabaseName,
		AccessMode:   neo4j.AccessModeRead,
	})
	if err != nil {
		// We don't have access to the internal database
		if strings.Contains(err.Error(), "Neo.ClientError.Security.Forbidden") {
			return ErrUnauthorized
		}
		// Bad credentials
		if strings.Contains(err.Error(), "Neo.ClientError.Security.Unauthorized") {
			return ErrUnauthorized
		}
	}
	defer session.Close()

	return
}

func verifyConnectivity(d neo4j.Driver, internalDatabaseName string) error {
	session := d.NewSession(neo4j.SessionConfig{AccessMode: neo4j.AccessModeRead, DatabaseName: internalDatabaseName})
	defer session.Close()
	result, err := session.Run("RETURN 1 AS n", nil)
	if err != nil {
		return err
	}
	_, err = result.Consume()
	return err
}
