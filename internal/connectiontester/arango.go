package connectiontester

import (
	"context"
	"crypto/tls"
	"database-connection-tester/internal/request"
	"errors"
	"fmt"
	"log"
	"strings"

	"github.com/arangodb/go-driver"
	arangohttp "github.com/arangodb/go-driver/http"
)

type arangoConnectionTester struct {
}

func (ct *arangoConnectionTester) TestConnection(ctx context.Context, req request.TestConnection) (err error) {
	conn, err := arangohttp.NewConnection(arangohttp.ConnectionConfig{
		Endpoints: []string{fmt.Sprintf("%s:%d", req.Hostname, req.Port)},
		// TODO: Use an actual TLS cert
		TLSConfig: &tls.Config{InsecureSkipVerify: true},
	})
	if err != nil {
		log.Println(err)
		return
	}

	client, err := driver.NewClient(driver.ClientConfig{
		Connection:     conn,
		Authentication: driver.BasicAuthentication(req.Username, req.Password),
	})
	if err != nil {
		log.Println(err)
		return
	}

	// Open the internal database
	_, err = client.Database(ctx, req.InternalDatabaseName)
	if err != nil {
		// If an invalid protocol is given, needs to be HTTP(S)
		if strings.Contains(err.Error(), "unsupported protocol scheme") {
			return ErrInvalidProtocol
		}
		// If the host can't be found
		if strings.Contains(err.Error(), "no such host") {
			return ErrBadAddress
		}
		// If the connection is refused
		if strings.Contains(err.Error(), "connection refused") {
			return ErrBadAddress
		}
		// If the connection times out, this is most likely a bad address
		if errors.Is(err, context.DeadlineExceeded) {
			return ErrBadAddress
		}

		arangoErr, ok := err.(driver.ArangoError)
		if !ok {
			return ErrUnknown
		}

		switch arangoErr.Code {
		case 401:
			return ErrUnauthorized
		default:
			log.Println(arangoErr.Code)
			log.Println(arangoErr.ErrorMessage)
			return ErrUnknown
		}
	}

	return
}
