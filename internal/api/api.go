package api

import (
	"log"
	"net/http"

	"github.com/go-playground/locales/en"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	en_translations "github.com/go-playground/validator/v10/translations/en"
)

// API describes the methods the API should have
type API interface {
	Start()
}

// api implements the API interface
type api struct {
	validator  *validator.Validate
	translator ut.Translator
}

// New creates a new API instance
func New() (a API, err error) {
	v := validator.New()

	// Register
	translator, err := registerValidationTranslations(v)
	if err != nil {
		return
	}

	a = &api{
		validator:  v,
		translator: translator,
	}

	return
}

func (a *api) Start() {
	log.Println("Started listening on port :3008")
	http.ListenAndServe(":3008", a.routes())
}

func registerValidationTranslations(v *validator.Validate) (translator ut.Translator, err error) {
	// Create translator
	en := en.New()
	uni := ut.New(en, en)
	translator, _ = uni.GetTranslator("en")

	// Register translator
	err = en_translations.RegisterDefaultTranslations(v, translator)
	if err != nil {
		return
	}

	// Add custom translation for required fields
	err = v.RegisterTranslation("required", translator, func(ut ut.Translator) error {
		return ut.Add("required", "{0} is a required field", true)
	}, func(ut ut.Translator, fe validator.FieldError) string {
		t, _ := ut.T("required", fe.Field())
		return t
	})
	if err != nil {
		return
	}

	// Add custom translation for 'Full Qualified Domain Name'
	err = v.RegisterTranslation("url", translator, func(ut ut.Translator) error {
		return ut.Add("url", "{0} must be a valid url", true)
	}, func(ut ut.Translator, fe validator.FieldError) string {
		t, _ := ut.T("url", fe.Field())
		return t
	})

	return
}
