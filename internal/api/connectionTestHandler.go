package api

import (
	"context"
	"database-connection-tester/internal/connectiontester"
	"database-connection-tester/internal/request"
	"net/http"
	"time"
)

func (a *api) connectionTestHandler(w http.ResponseWriter, r *http.Request) {
	// Grab the request from the context
	req := r.Context().Value(connectionTestRequestKey).(request.TestConnection)

	// Create a new connection tester
	connectionTester, err := connectiontester.New(req.DatabaseType)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	// Test the connection with a 45 second timeout
	connectionTestTimeout, connectionTestCancel := context.WithTimeout(context.Background(), time.Second*30)
	defer connectionTestCancel()

	err = connectionTester.TestConnection(connectionTestTimeout, req)
	if err != nil {
		// Invalid credentials
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	// Valid credentials
	w.WriteHeader(http.StatusOK)
}
