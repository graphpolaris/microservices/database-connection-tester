package api

import (
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/chi/v5"
)

func (a *api) routes() chi.Router {
	r := chi.NewRouter()

	// Add middleware
	r.Use(middleware.Recoverer)
	r.Use(middleware.Logger)

	// POST handler to receive incoming connection tests
	r.With(a.connectionRequestContext).Post("/", a.connectionTestHandler)

	return r
}
