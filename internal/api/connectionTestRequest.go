package api

import (
	"context"
	"database-connection-tester/internal/request"
	"encoding/json"
	"log"
	"net/http"

	"github.com/go-playground/validator/v10"
)

// requestContextKey is used to key items added to the request context
type requestContextKey int

const (
	connectionTestRequestKey requestContextKey = iota
)

// connectionRequestContext unmarshalls the request body into the ConnectionTestRequest struct
// and does some checks to make sure fields are filled and correctly formatted
func (a *api) connectionRequestContext(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		// Unmarshall request body
		var req request.TestConnection
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		// Validate the request
		err = a.validator.Struct(req)
		if err != nil {
			// Grab the first validation error
			err := err.(validator.ValidationErrors)[0]

			// Write response
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(err.Translate(a.translator)))
			return
		}

		// Request is valid, add to the request context
		ctx := context.WithValue(r.Context(), connectionTestRequestKey, req)

		// Serve next handler
		next.ServeHTTP(w, r.WithContext(ctx))
	}
	return http.HandlerFunc(fn)
}
