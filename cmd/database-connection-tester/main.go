package main

import "database-connection-tester/internal/api"

func main() {
	a, err := api.New()
	if err != nil {
		panic(err.Error())
	}
	a.Start()
}
